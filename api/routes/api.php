<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\TrajectoryController;
use App\Http\Controllers\UserController;
use \App\Http\Controllers\TestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login',[UserController::class, 'login']);
Route::middleware('auth:sanctum')->get('/user', [UserController::class, 'getCurrentUser']);

Route::group([ 'namespace' => 'App\Http\Controllers\Api' ], function () {
    Route::get('test', [TestController::class, 'getTest']);

    Route::group([ 'prefix' => 'users' ], function () {
        Route::get('',[UserController::class, 'getUsers']);
        Route::get('{id}',[UserController::class, 'getUser']);
        Route::get('add_score_tr/{id}/{trajectory_id}/{add_value}',
            [UserController::class, 'addScoreUserTrajectory']);
        Route::post('',[UserController::class, 'createUser']);
        Route::post('{id}',[UserController::class, 'updateUser']);
        Route::post('att_u_t/{id}',[UserController::class, 'attachUserTrajectory']);
        Route::post('det_u_t/{id}',[UserController::class, 'detachUserTrajectory']);
        Route::post('att_u_e/{id}',[UserController::class, 'attachUserEvent']);
        Route::post('det_u_e/{id}',[UserController::class, 'detachUserEvent']);
        Route::delete('{id}',[UserController::class, 'deleteUser']);
    });

    Route::group([ 'prefix' => 'tags' ], function () {
        Route::get('',[TagController::class, 'getTags']);
        Route::get('{id}',[TagController::class, 'getTag']);
        Route::post('',[TagController::class, 'createTag']);
        Route::post('{id}',[TagController::class, 'updateTag']);
        Route::delete('{id}',[TagController::class, 'deleteTag']);
    });

    Route::group([ 'prefix' => 'events' ], function () {
        Route::get('',[EventController::class, 'getEvents']);
        Route::get('{id}',[EventController::class, 'getEvent']);
        Route::get('show_similar/{id}',[EventController::class, 'showSimilarEvent']);
        Route::post('',[EventController::class, 'createEvent']);
        Route::post('{id}',[EventController::class, 'updateEvent']);
        Route::post('att_e_t/{id}',[EventController::class, 'attachEventTag']);
        Route::post('det_e_t/{id}',[EventController::class, 'detachEventTag']);
        Route::delete('{id}',[EventController::class, 'deleteEvent']);
    });

    Route::group([ 'prefix' => 'trajectories' ], function () {
        Route::get('',[TrajectoryController::class, 'getTrajectories']);
        Route::get('popular',[TrajectoryController::class, 'getPopularTrajectories']);
        Route::post('get_by_tags',[TrajectoryController::class, 'getTrajectoriesByTags']);
        Route::get('{id}',[TrajectoryController::class, 'getTrajectory']);
        Route::post('',[TrajectoryController::class, 'createTrajectory']);
        Route::post('{id}',[TrajectoryController::class, 'updateTrajectory']);
        Route::post('att_tr_e/{id}',[TrajectoryController::class, 'attachTrajectoryEvent']);
        Route::post('att_tr_t/{id}',[TrajectoryController::class, 'attachTagTrajectory']);
        Route::post('det_tr_e/{id}',[TrajectoryController::class, 'detachTrajectoryEvent']);
        Route::post('det_tr_t/{id}',[TrajectoryController::class, 'detachTrajectoryTag']);
        Route::delete('{id}',[TrajectoryController::class, 'deleteTrajectory']);
    });
});
