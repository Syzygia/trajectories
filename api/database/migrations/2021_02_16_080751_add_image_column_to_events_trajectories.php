<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageColumnToEventsTrajectories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->string('img_src')->nullable();
        });
        Schema::table('trajectories', function(Blueprint $table) {
           $table->string('img_src')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trajectories', function (Blueprint $table) {
            $table->dropColumn(['img_src']);
        });
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn(['img_src']);
        });
    }
}
