<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShortDescColTrajectoriesEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trajectories', function (Blueprint $table) {
            $table->string('short_desc')->nullable();
        });

        Schema::table('events', function (Blueprint $table) {
            $table->string('short_desc')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trajectories', function (Blueprint $table) {
            $table->dropColumn(['short_desc']);
        });
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn(['short_desc']);
        });
    }
}
