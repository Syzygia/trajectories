<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnTypesOnTrajectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trajectories', function (Blueprint $table) {
            $table->text('prizes')->nullable()->change();
            $table->text('forWhom')->nullable()->change();
            $table->text('advantages')->nullable()->change();
            $table->text('description')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trajectories', function (Blueprint $table) {
            $table->string('prizes')->nullable()->change();
            $table->string('forWhom')->nullable()->change();
            $table->string('advantages')->nullable()->change();
            $table->text('string')->nullable()->change();

        });
    }
}
