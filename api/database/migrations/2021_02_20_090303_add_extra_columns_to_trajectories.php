<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraColumnsToTrajectories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trajectories', function (Blueprint $table) {
            $table->string('forWhom')->nullable();
            $table->string('prizes')->nullable();
            $table->string('advantages')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trajectories', function (Blueprint $table) {
            $table->dropColumn(['forWhom', 'prize', 'advantages']);
        });
    }
}
