<?php

namespace App\Http\Controllers;

use App\Http\Resources\TagResource;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{
    const CREATE_VALIDATION_RULES = [
        'name' => 'required|string|max:255|',
    ];

    const UPDATE_VALIDATION_RULES = [
        'name' => 'required|string|max:255|unique:tags',
    ];

    public function getTags(Request $request)
    {
        $tags = Tag::all();
        return response()->json([
            //'data' => TagResource::collection($tags)
            'data' => TagResource::collection($tags)
        ], Response::HTTP_OK);
    }

    public function getTag(Request $request, int $id)
    {
        $tag = Tag::find($id);
        if (is_null($tag)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return response()->json([
            'data' => new TagResource($tag)
        ], Response::HTTP_OK);
    }

    public function createTag(Request $request)
    {
        $validator = Validator::make($request->all(), self::CREATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
            //return response()->json([Tag::])
        }
        $id = Tag::firstOrCreate($validator->validated())->id;
        return response()->json([$id], Response::HTTP_CREATED);
    }

    public function updateTag(Request $request, int $id)
    {
        $tag = Tag::find($id);
        if (is_null($tag)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $validator = Validator::make($request->all(), self::UPDATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $tag->update($validator->validated());
        return response()->json([], Response::HTTP_OK);
    }

    public function deleteTag(Request $request, int $id)
    {
        $tag = Tag::find($id);
        if (is_null($tag)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $tag->delete();
        return response()->json([], Response::HTTP_OK);
    }
}
