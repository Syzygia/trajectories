<?php

namespace App\Http\Controllers;

use App\Http\Resources\TrajectoryResource;
use App\Models\Event;
use App\Models\Tag;
use App\Models\Trajectory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TrajectoryController extends Controller
{
    const CREATE_VALIDATION_RULES = [
        'tittle' => 'required|string|max:255',
        'description' => 'required|string',
        'img_src' => 'nullable|string|max:1000',
        'forWhom' => 'nullable|string|max:1000',
        'prizes' => 'nullable|string|max:1000',
        'advantages' => 'nullable|string|max:255',
        'short_desc' => 'nullable|string|max:255'
    ];

    const UPDATE_VALIDATION_RULES = [
        'tittle' => 'nullable|string|max:255',
        'description' => 'string',
        'forWhom' => 'nullable|string|max:1000',
        'prizes' => 'nullable|string|max:1000',
        'advantages' => 'nullable|string|max:1000',
        'img_src' => 'nullable|string|max:255',
        'short_desc' => 'nullable|string|max:255',
        'edited' => 'boolean'
    ];

    public function getTrajectories(Request $request)
    {
        $trajectories = Trajectory::with(['events', 'tags'])->get();
        foreach ($trajectories as $trajectory) {
            views($trajectory)->record();
        }
        return response()->json([
            'data' =>TrajectoryResource::collection($trajectories)
        ], Response::HTTP_OK);
    }

    public function getTrajectory(Request $request, int $id)
    {
        $trajectory = Trajectory::with(['events', 'tags'])->find($id);
        if (is_null($trajectory)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        views($trajectory)->record();
        return response()->json([
            'data' =>new TrajectoryResource ($trajectory)
        ], Response::HTTP_OK);
    }

    public function createTrajectory(Request $request)
    {
        $validator = Validator::make($request->all(), self::CREATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $trajectory = $validator->validated();
        if ($trajectory['tittle'] == 'sample tittle') {
            $trajectory['edited'] = false;
        }
        $id = Trajectory::create($trajectory)->id;
        return response()->json([$id], Response::HTTP_CREATED);
    }

    public function updateTrajectory(Request $request, int $id)
    {
        $trajectory = Trajectory::find($id);
        if (is_null($trajectory)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $validator = Validator::make($request->all(), self::UPDATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $trajectory->update($validator->validated());
        return response()->json([], Response::HTTP_OK);
    }

    public function deleteTrajectory(Request $request, int $id)
    {
        $trajectory = Trajectory::find($id);
        if (is_null($trajectory)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $trajectory->delete();
        return response()->json([], Response::HTTP_OK);
    }

    public function attachTrajectoryEvent(Request $request, int $id)
    {
        $trajectory = Trajectory::find($id);
        if (is_null($trajectory)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('event_ids');
        foreach($ids as &$eventsId) {
            $event = Event::find($eventsId);
            if (is_null($event)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $trajectory->events()->syncWithoutDetaching($eventsId);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function detachTrajectoryEvent(Request $request, int $id)
    {
        $trajectory = Trajectory::find($id);
        if (is_null($trajectory)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('event_ids');
        foreach($ids as &$eventIds) {
            $event = Event::find($eventIds);
            if (is_null($event)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $trajectory->events()->detach($eventIds);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function attachTagTrajectory(Request $request, int $id)
    {
        $trajectory = Trajectory::find($id);
        if (is_null($trajectory)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('tag_ids');
        foreach($ids as &$tagsId) {
            $tag = Tag::find($tagsId);
            if (is_null($tag)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $trajectory->tags()->syncWithoutDetaching($tagsId);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function detachTrajectoryTag(Request $request, int $id)
    {
        $trajectory = Trajectory::find($id);
        if (is_null($trajectory)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('tag_ids');
        foreach($ids as &$tagIds) {
            $tag = Tag::find($tagIds);
            if (is_null($tag)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $trajectory->events()->detach($tagIds);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function getPopularTrajectories(Request $request)
    {
        $sorted = Trajectory::all()->sortByDesc('vote');
        $popular = [];
        $i = 0;

        foreach ($sorted as $item) {
            $popular [] = $item;
            views($item)->record();
            ++$i;
            if ($i  == 7){
                break;
            }
        }
        return response()->json(TrajectoryResource::collection($popular) , Response::HTTP_OK);
    }

    public function getTrajectoriesByTags(Request $request)
    {
        $ids = $request->input('tag_ids');
        foreach($ids as &$tagsId) {
            $tag = Tag::find($tagsId);
            if (is_null($tag)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
        }
        $trajectories = Trajectory::with('tags')->whereHas('tags', function($query) use ($ids){
            $query->whereIn('tag_id', $ids);
        })->get();
        return response()->json([
            TrajectoryResource::collection($trajectories)
        ], Response::HTTP_OK);
    }
}
