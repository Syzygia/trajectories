<?php

namespace App\Http\Controllers;

//use App\Http\Resources\UserResource;
use App\Http\Resources\UserLoginResource;
use App\Http\Resources\UserResource;
use App\Models\Event;
use App\Models\Trajectory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    const CREATE_VALIDATION_RULES = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|confirmed|max:255|min:8',
        'phone_num' => 'nullable|string|min:8|max:11',
        'whatsapp' => 'boolean',
        'phone' => 'boolean',
        'telegram' => 'boolean'
    ];

    const UPDATE_VALIDATION_RULES = [
        'name' => 'nullable|string|max:255',
        'description' => 'nullable',
        'is_tested' => 'boolean',
        'role' => 'string|max:15',
        'phone_num' => 'nullable|min:8|max:11',
        'whatsapp' => 'boolean',
        'phone' => 'boolean',
        'telegram' => 'boolean'
    ];

    public function getUsers(Request $request)
    {
        $users = User::with(['trajectories'])->get();
        return response()->json([
            'data' => UserResource::collection($users)
        ], Response::HTTP_OK);
    }

    public function getUser(Request $request, int $id)
    {
        $user = User::with(['trajectories'])->find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return response()->json([
            'data' => UserResource::collection(collect([$user]))->first()
        ], Response::HTTP_OK);
    }

    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), self::CREATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        User::create($validator->validated());
        return response()->json([], Response::HTTP_CREATED);
    }

    public function updateUser(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $validator = Validator::make($request->all(), self::UPDATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $user->update($validator->validated());
        return response()->json([], Response::HTTP_OK);
    }

    public function deleteUser(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $user->delete();
        return response()->json([], Response::HTTP_OK);
    }

    public function attachUserTrajectory(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('trajectory_ids');
        foreach($ids as &$trajectoriesId) {
            $trajectory = Trajectory::find($trajectoriesId);
            if (is_null($trajectory)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            if (!$user->trajectories->contains($trajectoriesId)) {
                $trajectory->increment('vote');
            }
            $user->trajectories()->syncWithoutDetaching($trajectoriesId);
        }
        return response()->json([], Response::HTTP_OK);
    }
    public function detachUserTrajectory(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('trajectory_ids');
        foreach($ids as &$trajectoriesId) {
            $trajectory = Trajectory::find($trajectoriesId);
            if (is_null($trajectory)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $trajectory->decrement('vote');
            $user->trajectories()->detach($trajectoriesId);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function attachUserEvent(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('event_ids');
        foreach($ids as &$eventsId) {
            $events = Event::find($eventsId);
            if (is_null($events)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $user->events()->syncWithoutDetaching($eventsId);
        }
        return response()->json([], Response::HTTP_OK);
    }
    public function detachUserEvent(Request $request, int $id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('event_ids');
        foreach($ids as &$eventsId) {
            $event = Event::find($eventsId);
            if (is_null($event)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $user->events()->detach($eventsId);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function addScoreUserTrajectory(Request $request, int $id, int $trajectoryId, int $addVal)
    {
        User::findOrFail($id)->first()->addTrajectoryScore($trajectoryId, $addVal);
        return response()->json([], Response::HTTP_OK);
    }
    public function login(Request $request)
    {
        $user = User::where($request->only(['email']))->first();
        if (!Hash::check($request->get('password'), $user->password)) {
            return response([ 'errors' => [ 'password' =>  'The password field is not correct.']]);
        }
        $token = $user->createToken('auth-token')->plainTextToken;
        return $token ? response('') ->header('Access-Control-Expose-Headers', 'Authorization')->
        withHeaders([ 'Authorization' => $token ]) :
            response() ->json([ 'message' => [ 'The token has not created.' ] ], 401);
    }
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json([], 200);
    }

    public function getCurrentUser(Request $request)
    {
        return new UserLoginResource($request->user());
    }

}
