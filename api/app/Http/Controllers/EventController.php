<?php

namespace App\Http\Controllers;

use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    const CREATE_VALIDATION_RULES = [
        'name' => 'required|string|max:255',
        'description' => 'nullable|string',
        'img_src' => 'nullable|string|max:255',
        'type' => 'nullable|string|max:255',
        'place' => 'nullable|string|max:255',
        'sponsor' => 'nullable|string|max:255',
        'date' => 'nullable|string|max:255',
        'advantages' => 'nullable|string|max:255',
        'short_desc' => 'nullable|string|max:255'
    ];

    const UPDATE_VALIDATION_RULES = [
        'name' => 'nullable|string|max:255',
        'description' => 'nullable|string',
        'img_src' => 'nullable|string|max:255',
        'date' => 'nullable|string|max:255',
        'type' => 'nullable|string|max:255',
        'place' => 'nullable|string|max:255',
        'sponsor' => 'nullable|string|max:255',
        'advantages' => 'nullable|string|max:255',
        'short_desc' => 'nullable|string|max:255'
    ];

    public function getEvents(Request $request)
    {
        $events = Event::with(['tags', 'trajectories'])->get();
        return response()->json([
            //'data' => UserResource::collection($users)
            'data' => EventResource::collection($events)
        ], Response::HTTP_OK);
    }

    public function getEvent(Request $request, int $id)
    {
        $event = Event::with(['tags', 'trajectories'])->find($id);
        if (is_null($event)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        return response()->json([
            'data' => new EventResource($event)
        ], Response::HTTP_OK);
    }

    public function createEvent(Request $request)
    {
        $validator = Validator::make($request->all(), self::CREATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $id = Event::create($validator->validated())->id;
        return response()->json([$id], Response::HTTP_CREATED);
    }

    public function updateEvent(Request $request, int $id)
    {
        $event = Event::find($id);
        if (is_null($event)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $validator = Validator::make($request->all(), self::UPDATE_VALIDATION_RULES);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], Response::HTTP_FORBIDDEN);
        }
        $event->update($validator->validated());
        return response()->json([], Response::HTTP_OK);
    }

    public function deleteEvent(Request $request, int $id)
    {
        $event = Event::find($id);
        if (is_null($event)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $event->delete();
        return response()->json([], Response::HTTP_OK);
    }

    public function attachEventTag(Request $request, int $id)
    {
        $event = Event::find($id);
        if (is_null($event)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('tag_ids');
        foreach($ids as &$tagsId) {
            $tag = Tag::find($tagsId);
            if (is_null($tag)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $event->tags()->syncWithoutDetaching($tagsId);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function detachEventTag(Request $request, int $id)
    {
        $event = Event::find($id);
        if (is_null($event)) {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
        $ids = $request->input('tag_ids');
        foreach($ids as &$tagsId) {
            $tag = Tag::find($tagsId);
            if (is_null($tag)) {
                return response()->json([], Response::HTTP_NOT_FOUND);
            }
            $event->tags()->detach($tagsId);
        }
        return response()->json([], Response::HTTP_OK);
    }

    public function showSimilarEvent(Request $request, int $eventId)
    {
        $target_event = Event::findOrFail($eventId)->first();
        $ids = $target_event->tags()->pluck('tag_id');
        $similar_events = Event::with('tags')->whereHas('tags', function($query) use ($ids, $eventId){
            $query->whereIn('tag_id', $ids)->where('events.id','!=', $eventId);
        })->get();
        return response()->json([
            $similar_events
        ], Response::HTTP_OK);
    }
}
