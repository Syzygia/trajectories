<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'date' => $this->date,
            'img_src' => $this->img_src,
            'tags' => TagResource::collection($this->whenLoaded('tags')),
            'type' => $this->type,
            'place' => $this->place,
            'sponsor' => $this->sponsor,
            'advantages' => explode('/', $this->advantages),
            'short_desc' => $this->short_desc,
            'trajectories' => TrajectoryResource::collection($this->whenLoaded('trajectories')),
        ];
    }
}
