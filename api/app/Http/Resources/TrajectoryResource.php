<?php

namespace App\Http\Resources;

use App\Models\Trajectory;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class TrajectoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $id = $this->id;
        $users_top = [];

        $users = DB::table('trajectory_user')->join('trajectories', 'trajectories.id', '=', 'trajectory_user.trajectory_id')
            ->join('users', 'users.id', '=', 'trajectory_user.user_id')
            ->orderBy('trajectory_user.score', 'DESC')
            ->select('users.*', 'trajectory_user.score')->get();
        return [
              'id' => $this->id,
              'tittle' => $this->tittle,
              'description' => $this->description,
              'img_src' => $this->img_src,
              'tags' => TagResource::collection($this->whenLoaded('tags')),
              'events' => EventResource::collection($this->whenLoaded('events')),
              'advantages' => explode('/', $this->advantages),
              'forWhom' => explode('/', $this->forWhom),
              'prizes' => explode('/', $this->prizes),
              'table' => $users,
              //score' => $score,
              'short_desc' => $this->short_desc,
              'vote' => $this->vote,
              'edited' => $this->edited
        ];
    }
}
