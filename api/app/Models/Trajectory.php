<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;

class Trajectory extends Model implements Viewable
{
    use InteractsWithViews;
    use HasFactory;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'trajectories';
    protected $guarded = ['id'];
    protected $fillable = [
        'tittle',
        'description',
        'img_src',
        'forWhom',
        'prizes',
        'advantages',
        'short_desc',
        'edited'
    ];
    protected $attributes = [
        'vote' => 0
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function increaseEventPriority(int $id, int $addVal){
        return $this->events()->findOrFail($id)->pivot->increment('priority', $addVal);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tag_trajectory', 'trajectory_id',
            'tag_id');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_trajectory', 'trajectory_id',
            'event_id')->withPivot('priority')->orderBy('priority');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'trajectory_user', 'trajectory_id',
            'user_id')->withPivot('score');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
