<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'users';
    protected $guarded = ['id'];
    protected $fillable = [
        'email',
        'password',
        'name',
        'role',
        'is_tested',
        'phone',
        'phone_num',
        'whatsapp',
        'telegram',
];
    protected $hidden = ['password'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function addTrajectoryScore(int $id, int $addVal){
        $this->trajectories()->findOrFail($id)->pivot->increment('score', $addVal);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function trajectories()
    {
        return $this->belongsToMany(Trajectory::class, 'trajectory_user', 'user_id',
            'trajectory_id')->withPivot('score');
    }

    public function events()
    {
        return $this->belongsToMany(Trajectory::class, 'event_user', 'user_id',
            'event_id')->withPivot(['is_completed']);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
