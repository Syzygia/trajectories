<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'events';
    protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'description',
        'img_src',
        'type',
        'date',
        'place',
        'sponsor',
        'advantages',
        'short_desc'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'event_tag', 'event_id',
            'tag_id');
    }

    public function trajectories()
    {
        return $this->belongsToMany(Trajectory::class, 'event_trajectory', 'event_id',
            'trajectory_id')->withPivot('priority')->orderBy('priority');
    }

    public function users()
    {
        return $this->belongsToMany(Trajectory::class, 'event_user', 'event_id',
            'user_id')->withPivot(['is_completed']);
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
