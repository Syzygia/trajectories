import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {layout: 'main'},
    component: () => import('@/views/Home.vue')
  },
  {
    path: '/login',
    name: 'login',
    meta: {layout: 'minor'},
    component: () => import('@/views/Login.vue')
  },
  {
    path: '/events',
    name: 'events',
    meta: {layout: 'main'},
    component: () => import('@/views/Events.vue')
  },
  {
    path: '/trajectory',
    name: 'trajectory',
    meta: {layout: 'main'},
    component: () => import('@/views/Trajectory.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
